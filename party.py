# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys

from trytond.model import ModelView, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Not, Bool


class Party(metaclass=PoolMeta):
    __name__ = 'party.party'

    identified_renaper = fields.Boolean('Identified RENAPER', readonly=True,
        states={'invisible': Not(Bool(Eval('is_person')))},
        help='Shows if party info was checked in RENAPER system')
    renaper_id = fields.Char('Renaper ID')
    identified_sisa = fields.Boolean('In SISA Census', readonly=True,
        states={'invisible': Not(Bool(Eval('is_person')))},
        help='Shows if party is in SISA Census')
    sisa_code = fields.Char('SISA Code')
    date_of_deceased = fields.Date('Fecha de fallecimiento',
        readonly = True,
        states={'invisible':Not(Bool(Eval('deceased')))})

    @classmethod
    def __setup__(cls):
        super(Party, cls).__setup__()
        cls._buttons.update({
            'get_census_data': {
                'invisible': Not(Bool(Eval('is_person'))),
                },
        })
        cls.name.states['readonly'] = Eval('identified_renaper')    
        cls.lastname.states['readonly'] = Eval('identified_renaper')
        cls.ref.states['readonly'] = Eval('identified_renaper')
        cls.gender.states['readonly'] = Eval('identified_renaper')
        cls.dob.states['readonly'] = Eval('identified_renaper')

    @staticmethod
    def default_identified_renaper():
        return False

    @staticmethod
    def default_identified_sisa():
        return False

    @classmethod
    def _vat_types(cls):
        vat_types = super(Party, cls)._vat_types()
        if 'ar_dni' not in vat_types:
            vat_types.append('ar_dni')
        return vat_types

    @classmethod
    @ModelView.button_action('health_sisa_census.wizard_census_data')
    def get_census_data(cls, parties):
        pass


class Identifier(metaclass=PoolMeta):
    __name__ = 'party.identifier'

    @classmethod
    def get_types(cls):
        types = super(Identifier, cls).get_types()
        if ('ar_dni', 'DNI') not in types:
            types.append(('ar_dni', 'DNI'))
        return types

    #@classmethod
    #def get_types(cls):
        #return super().get_types + [('ar_rnos', 'RNOS')]


class PartyAddress(metaclass=PoolMeta):
    'Party Address'
    __name__ = 'party.address'

    legal_address = fields.Boolean("Legal address")

    def update_direccion(self, party, data):
        self.name = 'Domicilio legal'
        self.street = data.domicilio+ ' '+ data.piso_depto
        self.city = data.localidad
        self.postal_code = data.codigo_postal
        self.subdivision = self._get_provincia(data.provincia)
        self.country = self._get_pais()
        self.party = party
        self.legal_address = True
        self.save()

    def _get_provincia(self, provincia):
        Subdivision = Pool().get('country.subdivision')

        provincias_match = {
            'Córdoba': 'Cordoba',
            'Entre Ríos': 'Entre Rios',
            'Neuquén': 'Neuquen',
            'Río Negro': 'Rio Negro',
            'Tucumán': 'Tucuman',
            }
        provincia = provincias_match.get(provincia, provincia)
        subdivision = Subdivision().search([
            ('code', 'like', 'AR%'),
            ('name', '=', provincia),
        ])
        return subdivision[0] if subdivision else None

    def _get_pais(self):
        Country = Pool().get('country.country')

        country = Country().search(
            ['code', '=', 'AR']
        )
        return country[0] if country else None
