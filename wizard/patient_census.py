# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
import sys

from datetime import date

from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, StateTransition, Button
from trytond.pool import Pool
from trytond.transaction import Transaction
from trytond.pyson import Eval, Bool, Not
from trytond.i18n import gettext

from trytond.modules.health_sisa_puco.puco_ws import PucoWS
from ..census_ws import CensusWS
from ..exceptions import (ErrorWS, ErrorAutenticacion, ErrorInesperado,
    NoCuotaDisponible, ErrorDatos, RegistroNoEncontrado,
    ServicioRENAPERNoDisponible, MultipleResultado, MoreThanOnePatient,
    UnknownError)

class PatientCensusDataStart(ModelView):
    'Patient Census Data Start'
    __name__ = 'patient.census_data.start'

    ref = fields.Char('PUID', readonly=True)
    identificado_renaper = fields.Char('RENAPER code', readonly=True)
    padron_sisa = fields.Char('SISA census', readonly=True)
    codigo_sisa = fields.Char('SISA code', readonly=True)#
    tipo_documento = fields.Char('ID type', readonly=True)#
    nro_documento = fields.Char('ID number', readonly=True)#
    apellido = fields.Char('Lastname', readonly=True)
    nombre = fields.Char('Name', readonly=True)
    sexo = fields.Char('Sex', readonly=True)
    fecha_nacimiento = fields.Date('Birth date', readonly=True)
    estado_civil = fields.Char('Marital status', readonly=True)
    provincia = fields.Char('Province', readonly=True)
    departamento = fields.Char('Subdivision', readonly=True)
    localidad = fields.Char('City', readonly=True)
    domicilio = fields.Char('Address', readonly=True)
    piso_depto = fields.Char('Apartment', readonly=True)
    codigo_postal = fields.Char('Zip code', readonly=True)
    pais_nacimiento = fields.Char('Birth country', readonly=True)
    provincia_nacimiento = fields.Char('Birth province', readonly=True)
    localidad_nacimiento = fields.Char('Birth city', readonly=True)
    nacionalidad = fields.Char('Nationality', readonly=True)
    fallecido = fields.Char('Deceased', readonly=True)
    fecha_fallecido = fields.Date('Fecha de Deceso', readonly=True)
    cobertura = fields.One2Many('get.socialsecurity.view',None,'Cobertura',
        readonly=True)
    obra_social_elegida = fields.Many2One('party.party','Obra Social Elegida',
        domain=[('id', 'in', Eval('obra_social_domain'))])
    obra_social_domain = fields.Function(
        fields.Many2Many('party.party',None, None, 'Obra social Domain'),
                    'on_change_with_obra_social_domain')
    url_padron = fields.Char(u'Buscar en el padron web', readonly=True)
    select_du = fields.Selection([
        ('',''),
        ('yes',u'Sí'),
        ('no','No'),
        ],'Utilizar ReNaPer para UD',
        help=u'Utilizar los datos provistos por\n'
        u'ReNaPer para la Unidad Domiciliaria',required=True)

    def get_obra_social_domain(self, name=None):
        Party = Pool().get('party.party')
        rnos_list = [c.rnos for c in self.cobertura]
        obras_sociales = Party.search([
            ('is_insurance_company','=',True),
            ('identifiers.code','in', rnos_list),
            ])
        nombreObraSocial = [c.nombreObraSocial for c in self.cobertura]
        obras_sociales+= Party.search([
            ('is_insurance_company','=',True),
            ('name','in',nombreObraSocial)])
        obras_sociales = list(set(obras_sociales))
        return [os.id for os in obras_sociales]

    @fields.depends('cobertura')
    def on_change_with_obra_social_domain(self, name=None):
        Party = Pool().get('party.party')
        rnos_list = [c.rnos for c in self.cobertura]
        obras_sociales = Party.search([
            ('is_insurance_company','=',True),
            ('identifiers.code','in', rnos_list),
            ])
        nombreObraSocial = [c.nombreObraSocial for c in self.cobertura]
        obras_sociales += Party.search([
            ('is_insurance_company','=',True),
            ('name','in',nombreObraSocial)])
        obras_sociales = list(set(obras_sociales))
        return [os.id for os in obras_sociales]
    
    @staticmethod
    def default_url_padron():
        return 'www.saludnqn.gob.ar/PadronConsultasWeb/'


class PatientCensusData(Wizard):
    'Patient Census Data'
    __name__ = 'patient.census_data'

    start = StateView(
        'patient.census_data.start',
        'health_sisa_census.patient_census_data_start_view', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Ok', 'update_patient', 'tryton-ok', default=True),
        ])
    update_patient = StateTransition()

    def default_start(self, fields):
        Party = Pool().get('party.party')
        Patient = Pool().get('gnuhealth.patient')
        res = {}
        if len(Transaction().context['active_ids']) > 1:
            raise MoreThanOnePatient(gettext('msg_more_than_one_patient'))
        patient = Patient(Transaction().context['active_id'])
        if not patient.name:
            return res

        xml = None
        if patient.name.gender:
            xml = CensusWS.get_xml(patient.name.ref,patient.name.gender)
        else:
            xml = CensusWS.get_xml(patient.name.ref)
        xml_puco = PucoWS.get_xml(patient.name.ref)
        if xml is None:
            raise ErrorWS(gettext('msg_error_ws'))
        xml_result = xml.findtext('resultado')
        if xml_result == 'OK':
            cobertura = []
            osocial = {}
            for cober in xml.findall('cobertura'):
                osocial['tipoCobertura'] = cober.findtext('tipoCobertura')
                osocial['nombreObraSocial'] = cober.findtext('nombreObraSocial')
                osocial['rnos'] = cober.findtext('rnos')
                osocial['vigenciaDesde '] = cober.findtext('vigenciaDesde')
                osocial['fechaActualizacion'] = cober.findtext('fechaActualizacion')
                osocial['denominacion'] = cober.findtext('denominacion')
                osocial['procedencia'] = 'padron'
                cobertura.append(osocial)
                osocial = {}
            if xml_puco and xml_puco.findtext('resultado') == 'OK':
                for cober in xml_puco.findall('puco'):
                    osocial['tipoCobertura'] = cober.findtext('tipoCobertura')
                    osocial['nombreObraSocial'] = cober.findtext('coberturaSocial')
                    osocial['rnos'] = cober.findtext('rnos')
                    osocial['vigenciaDesde'] = cober.findtext('vigenciaDesde')
                    osocial['fechaActualizacion'] = cober.findtext('fechaActualizacion')
                    osocial['denominacion'] = cober.findtext('denominacion')
                    osocial['procedencia'] = 'puco'
                    cobertura.append(osocial)
                    osocial = {}
            obra_social_elegida = None
            if cobertura and\
                (sum([x['rnos'] == cobertura[0]['rnos'] for x in cobertura]) == len(cobertura)):
                os = Party.search([
                    ('is_insurance_company','=',True),
                    ('identifiers.code','=', cobertura[0]['rnos'])
                    ])
                if  os:
                    obra_social_elegida = os[0].id
            res = {
                'identificado_renaper': xml.findtext('identificadoRenaper'),
                'padron_sisa': xml.findtext('PadronSISA'),
                'codigo_sisa': xml.findtext('codigoSISA'),
                'tipo_documento': xml.findtext('tipoDocumento'),
                'nro_documento': xml.findtext('nroDocumento'),
                'apellido': xml.findtext('apellido'),
                'nombre': xml.findtext('nombre'),
                'sexo': xml.findtext('sexo'),
                'fecha_nacimiento': xml.findtext('fechaNacimiento'),
                'estado_civil': xml.findtext('estadoCivil'),
                'provincia': xml.findtext('provincia'),
                'departamento':xml.findtext('departamento'),
                'localidad': xml.findtext('localidad'),
                'domicilio': xml.findtext('domicilio'),
                'piso_depto': xml.findtext('pisoDpto'),
                'codigo_postal': xml.findtext('codigoPostal'),
                'pais_nacimiento': xml.findtext('paisNacimiento'),
                'provincia_nacimiento': xml.findtext('provinciaNacimiento'),
                'localidad_nacimiento': xml.findtext('localidadNacimiento'),
                'nacionalidad': xml.findtext('nacionalidad'),
                'fallecido': xml.findtext('fallecido'),
                'fecha_fallecido': xml.findtext('fechaFallecido'),
                'cobertura': cobertura,
                'obra_social_elegida': obra_social_elegida,
            }
        elif xml_result == 'ERROR_AUTENTICACION':
            raise ErrorAutenticacion(gettext('msg_error_autenticacion'))
        elif xml_result == 'ERROR_INESPERADO':
            raise ErrorInesperado(gettext('msg_error_inesperado'))
        elif xml_result == 'NO_TIENE_QUOTA_DISPONIBLE':
            raise NoCuotaDisponible(gettext('msg_no_cuota_disponible'))
        elif xml_result == 'ERROR_DATOS':
            raise ErrorDatos(gettext('msg_error_datos'))
        elif xml_result == 'REGISTRO_NO_ENCONTRADO':
            raise RegistroNoEncontrado(gettext('msg_registro_no_encontrado'))
        elif xml_result == 'SERVICIO_RENAPER_NO_DISPONIBLE':
            raise ServicioRENAPERNoDisponible(
                gettext('msg_servicio_renaper_no_disponible'))
        elif xml_result == 'MULTIPLE_RESULTADO':
            raise MultipleResultado(gettext('msg_multiple_resultado'))
        else:
            raise UnknownError(gettext('msg_unknown_error'))
        return res

    def transition_update_patient(self):
        pool = Pool()
        Party = pool.get('party.party')
        Address = pool.get('party.address')
        Country = pool.get('country.country')
        Insurance = pool.get('gnuhealth.insurance')
        Patient = pool.get('gnuhealth.patient')
        patient = Patient(Transaction().context['active_id'])
        party = patient.name
        identifier_data = {
            'type': 'ar_dni' if self.start.tipo_documento == 'DNI' else None,
            'code': self.start.nro_documento,
            }
        party.name = self.start.nombre
        party.lastname = self.start.apellido
        party.ref = self.start.nro_documento
        if not party.identifiers:
            party.identifiers+= (identifier_data),
        party.dob = self.start.fecha_nacimiento
        party.gender = self.start.sexo.lower() if self.start.sexo else 'm'
        party.identified_renaper = True
        party.non_identified_renaper = False
        party.last_sisa_census_check = date.today()
        party.renaper_id = self.start.identificado_renaper
        party.is_person = True
        party.is_patient = True

        if self.start.padron_sisa == 'SI':
            party.identified_sisa = True
            party.sisa_code = self.start.codigo_sisa
        if 'trytond.modules.party_ar' in sys.modules:
            party.iva_condition = 'consumidor_final'

        if self.start.pais_nacimiento:
            country = Country.search(
                ['OR',
                ('name', 'ilike', self.start.pais_nacimiento),
                ('name', 'ilike', self.start.nacionalidad),
                ])
            if country:
                party.citizenship = country[0].id

        if self.start.domicilio:
            direccion = Address().search([
                ('party', '=', party.id),
                ])
            if direccion and (direccion[0].street is None
                    or direccion[0].street == ''):
                direccion[0].update_direccion(party, self.start)
            else:
                direccion = Address()
                direccion.update_direccion(party, self.start)

        insurance = None
        if self.start.obra_social_elegida:
            insurance_party = Party().search([
                ('id', '=', self.start.obra_social_elegida.id),
                ])[0]
            if insurance_party:
                insurance = Insurance().search([
                    ('name', '=', party.id),
                    ('company', '=', insurance_party.id),
                    ])
                if not insurance:
                    insurance_data = {
                        'name': party.id,
                        'number': self.start.nro_documento,
                        'company': insurance_party.id,
                        'insurance_type':
                            insurance_party.insurance_company_type,
                        }
                    insurance = Insurance.create([insurance_data])
            if insurance:
                if not patient.current_insurance:
                    patient.current_insurance = insurance[0]
                    patient.save()

        #insurance_party = Party().search([
            #('is_insurance_company', '=', True),
            #('identifiers.type', '=', 'ar_rnos'),
            #('identifiers.code', '=', self.start.rnos),
            #])
        #if not insurance_party:
            #insurance_party = Party().search([
                #('is_insurance_company', '=', True),
                #('name', '=', self.start.obra_social_elegida),
                #])

        #if insurance_party:
            #insurance = Insurance().search([
                #('name', '=', party.id),
                #('company', '=', insurance_party.id),
                #])
            #if not insurance:
                #insurance_data = {
                    #'name': party.id,
                    #'number': numero,
                    #'company': insurance_party.id,
                    #'insurance_type':
                        #insurance_party.insurance_company_type,
                    ##'member_since': data.vigencia_os,
                    #}
                #insurance = Insurance.create([insurance_data])
            #else:
                #insurance[0].number = numero
                #insurance[0].company = insurance_party.id
                #insurance[0].insurance_type = insurance_party.insurance_company_type
                ##insurance[0].member_since = self.start.vigencia_os
                #insurance[0].save()

        party.deceased = True if self.start.fallecido == 'SI' else False
        party.date_of_deceased = self.start.fecha_fallecido
        party.save()

        return 'end'
