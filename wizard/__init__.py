# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from . import batch_patient_census
from . import party_census
from . import patient_census
from . import patient_create


