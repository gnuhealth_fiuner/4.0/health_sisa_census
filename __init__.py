# -*- coding: utf-8 -*-
#This file is part health_sisa_census module for Tryton.
#The COPYRIGHT file at the top level of this repository contains
#the full copyright notices and license terms.
from trytond.pool import Pool
from . import party
from . import health
from .wizard import batch_patient_census
from .wizard import party_census
from .wizard import patient_census
from .wizard import patient_create


def register():
    Pool.register(
        party.Party,
        party.Identifier,
        party.PartyAddress,
        health.PartyPatient,
        health.PatientData,
        health.Appointment,
        patient_census.PatientCensusDataStart,
        party_census.CensusDataStart,
        patient_create.PatientCreateStart,
        patient_create.PatientCreateFound,
        patient_create.PatientCreateManual,
        patient_create.PatientCreateData,
        batch_patient_census.SocialSecurity,
        batch_patient_census.BatchPatientCensusDataStart,
        batch_patient_census.BatchPatientCensusPrevalidation,
        batch_patient_census.BatchPatientCensusPrevalidationList,
        batch_patient_census.BatchPatientCensusValidation,
        batch_patient_census.BatchPatientCensusValidationList,
        module='health_sisa_census', type_='model')
    Pool.register(
        party_census.CensusData,
        patient_create.PatientCreate,
        patient_census.PatientCensusData,
        batch_patient_census.BatchPatientCensusData,
        module='health_sisa_census', type_='wizard')
